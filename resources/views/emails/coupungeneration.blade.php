<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Proline Emailer</title>
    <style type="text/css">
        html, body{margin: 0;padding: 0}
    </style>
</head>
<body>

<table width="650" cellspacing="0" cellpadding="0" align="center" style="font-family: 'Gotham', sans-serif;background:#d1d3d4;color:#000000;padding:50px 20px;">
    <tr>
        <td colspan="4" width="100%">
            <a href="https://www.prolineindia.com/" target="_blank"><img src="http://dev.firsteconomy.com/proline_game/img/logo.png" usemap="#image-map" style="display: block;margin:0 auto;padding-bottom: 50px;"></a>
            <h1>Congrats!</h1>
            <h3>CouponCode: {{$data['couponid']}}</h3>
            {{--<h3 style="line-height: 1.5;">Thank you for playing. You have received a voucher of Rs. {{$data['discount']}}. This voucher can be used at the Proline Store mentioned in the Terms and Conditions 22nd September 2019.</h3>--}}
            <h3>*Congrats!*</h3>
            <h3>
                Thank you for playing. You have received a voucher of Rs. {{$data['discount']}}. This voucher can be redeemed at ProlineIndia.com or Proline Store mentioned in the Terms and Conditions by 22nd September 2019. <a href="http://game.prolineindia.com/termandconditions">Link</a> </h3>
        </td>
    </tr>
    </table>
</body>
</html>